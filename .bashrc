#
# ~/.bashrc
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias cat='bat --style header --style rules --style snip --style changes --style header'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
# Replace ls with exa
alias ls='exa -al --color=always --group-directories-first --icons' # preferred listing
alias la='exa -a --color=always --group-directories-first --icons'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first --icons'  # long format
alias lt='exa -aT --color=always --group-directories-first --icons' # tree listing
alias l.="exa -a | egrep '^\.'"                                     # show only dotfiles
PS1='\[\e[1m\]\[\e[38;5;200m\]\u\[\e[38;5;2m\]@\[\e[5m\]\[\e[38;5;6m\]\h\[\e[0m\]\[\e[1m\]\[\e[38;5;86m\] \d\[\e[38;5;253m\]  \@\[\e[38;5;7m\] \W]\$\[\e[0m\] '
#export PS1="\[\e[31m\][\[\e[m\]\[\e[38;5;172m\]\u\[\e[m\]@\[\e[38;5;153m\]\h\[\e[m\] \[\e[38;5;214m\]\W\[\e[m\]\[\e[31m\]]\[\e[m\]\\$ "

# cat<<'EOF'
#            _..._
#          .'     '.
#         /  _   _  \
#         | (o)_(o) |
#          \(     ) /
#          //'._.'\ \
#         //   .   \ \
#        ||   .     \ \
#        |\   :     / |
#        \ `) '   (`  /_
#      _)``".____,.'"` (_
#      )     )'--'(     (
#       '---`      `---`
# EOF
## Run paleofetch
neofetch | lolcat
#eval "$(starship init bash)"
